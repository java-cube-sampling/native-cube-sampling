package cube.nativeimplementation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NativeImplementationApplication {

	public static void main(String[] args) {
		SpringApplication.run(NativeImplementationApplication.class, args);
	}

}
